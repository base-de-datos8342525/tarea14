# Abastecimiento
```
CREATE TRIGGER trg_AbastecimientoChanges
ON ABASTECIMIENTO
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    IF EXISTS (SELECT * FROM INSERTED)
    BEGIN
        INSERT INTO ABASTECIMIENTO (COD_PRV, COD_PRO, PRE_ABA, OPERATION)
        SELECT COD_PRV, COD_PRO, PRE_ABA, 'INSERT'
        FROM INSERTED;
    END

    IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
    BEGIN
        INSERT INTO ABASTECIMIENTO (COD_PRV, COD_PRO, PRE_ABA, OPERATION)
        SELECT COD_PRV, COD_PRO, PRE_ABA, 'UPDATE'
        FROM INSERTED;
    END

    IF EXISTS (SELECT * FROM DELETED)
    BEGIN
        INSERT INTO ABASTECIMIENTO (COD_PRV, COD_PRO, PRE_ABA, OPERATION)
        SELECT COD_PRV, COD_PRO, PRE_ABA, 'DELETE'
        FROM DELETED;
    END
END
GO

```